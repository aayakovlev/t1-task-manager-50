package ru.t1.aayakovlev.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class UserServiceImplTest extends AbstractSchemeTest {

    @NotNull
    private static PropertyService propertyService;

    @NotNull
    private static ConnectionService connectionService;

    @NotNull
    private static UserDTOService service;

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
        liquibase.dropAll();
        liquibase.update("scheme");

        propertyService = new PropertyServiceImpl();
        connectionService = new ConnectionServiceImpl(propertyService);
        service = new UserDTOServiceImpl(connectionService, propertyService);
    }

    @AfterClass
    public static void destroyConnection() {
        connectionService.close();
    }

    @Before
    public void initData() throws AbstractException {
        service.save(COMMON_USER_ONE);
    }

    @After
    public void afterClear() throws AbstractException {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnUser() throws AbstractException {
        @Nullable final UserDTO user = service.findById(COMMON_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(COMMON_USER_ONE.getEmail(), user.getEmail());
        Assert.assertEquals(COMMON_USER_ONE.getFirstName(), user.getFirstName());
        Assert.assertEquals(COMMON_USER_ONE.getLastName(), user.getLastName());
        Assert.assertEquals(COMMON_USER_ONE.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(COMMON_USER_ONE.getLogin(), user.getLogin());
    }

    @Test
    public void When_FindByIdNotExistsUser_Expect_ReturnNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(USER_ID_NOT_EXISTED));
    }

    @Test
    public void When_SaveNotNullUser_Expect_ReturnUser() throws AbstractException {
        @NotNull final UserDTO savedUser = service.save(ADMIN_USER_ONE);
        Assert.assertNotNull(savedUser);
        Assert.assertEquals(ADMIN_USER_ONE, savedUser);
        @Nullable final UserDTO user = service.findById(ADMIN_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER_ONE.getId(), user.getId());
        Assert.assertEquals(ADMIN_USER_ONE.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_USER_ONE.getEmail(), user.getEmail());
        Assert.assertEquals(ADMIN_USER_ONE.getRole(), user.getRole());
        Assert.assertEquals(ADMIN_USER_ONE.getFirstName(), user.getFirstName());
        Assert.assertEquals(ADMIN_USER_ONE.getLastName(), user.getLastName());
        Assert.assertEquals(ADMIN_USER_ONE.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(ADMIN_USER_ONE.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void When_RemoveExistedUser_Expect_ReturnUser() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE));
        service.removeById(ADMIN_USER_ONE.getId());
    }

    @Test
    public void When_RemoveAll_Expect_NullUsers() throws AbstractException {
        service.save(ADMIN_USER_ONE);
        service.save(ADMIN_USER_TWO);
        service.clear();
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(ADMIN_USER_ONE.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(ADMIN_USER_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdExistedUser_Expect_User() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE));
        service.removeById(ADMIN_USER_ONE.getId());
    }

    @Test
    public void When_RemoveByIdNotExistedUser_Expect_ThrowsEntityNotFoundException() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.removeById(USER_ID_NOT_EXISTED)
        );
    }

    @Test
    public void When_FindByLogin_Expect_User() throws AbstractException {
        Assert.assertNotNull(service.findByLogin(COMMON_USER_ONE.getLogin()));
    }

    @Test
    public void When_FindByEmail_Expect_User() throws AbstractException {
        Assert.assertNotNull(service.findByEmail(COMMON_USER_ONE.getEmail()));
    }

    @Test
    public void When_IsLoginExists_Expect_True() throws AbstractException {
        Assert.assertTrue(service.isLoginExists(COMMON_USER_ONE.getLogin()));
    }

    @Test
    public void When_IsEmailExist_Expect_True() throws AbstractException {
        Assert.assertTrue(service.isEmailExists(COMMON_USER_ONE.getEmail()));
    }

    @Test
    public void When_LockUserByLogin_Expect_LockedUser() throws AbstractException {
        service.save(ADMIN_USER_ONE);
        @NotNull final UserDTO user = service.lockUserByLogin(ADMIN_USER_ONE.getLogin());
        Assert.assertTrue(user.isLocked());
    }

    @Test
    public void When_RemoveByLogin_Expect_RemovedUser() throws AbstractException {
        service.save(ADMIN_USER_TWO);
        service.removeByLogin(ADMIN_USER_TWO.getLogin());

    }

    @Test
    public void When_RemoveByEmail_Expect_RemovedUser() throws AbstractException {
        service.save(ADMIN_USER_TWO);
        service.removeByEmail(ADMIN_USER_TWO.getEmail());
    }

    @Test
    public void When_SetPassword_Expect_ChangedPassword() throws AbstractException {
        @NotNull final UserDTO user = service.setPassword(COMMON_USER_ONE.getId(), PASSWORD);
        Assert.assertEquals(PASSWORD_HASH, user.getPasswordHash());
    }

    @Test
    public void When_UnlockUserByLogin_Expect_UnlockedUser() throws AbstractException {
        service.save(ADMIN_USER_ONE);
        service.lockUserByLogin(ADMIN_USER_ONE.getLogin());
        @NotNull final UserDTO user = service.unlockUserByLogin(ADMIN_USER_ONE.getLogin());
        Assert.assertFalse(user.isLocked());
    }

    @Test
    public void When_UpdateUser_Expect_UpdatedUser() throws AbstractException {
        service.save(ADMIN_USER_ONE);
        @NotNull final UserDTO user = service.update(ADMIN_USER_ONE.getId(), FIRST_NAME, MIDDLE_NAME, LAST_NAME);
        Assert.assertEquals(FIRST_NAME, user.getFirstName());
        Assert.assertEquals(LAST_NAME, user.getLastName());
        Assert.assertEquals(MIDDLE_NAME, user.getMiddleName());
    }

}
