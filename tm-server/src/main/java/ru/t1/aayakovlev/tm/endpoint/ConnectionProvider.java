package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;

public interface ConnectionProvider {

    @NotNull
    String getHost();

    @NotNull
    String getPort();

}
