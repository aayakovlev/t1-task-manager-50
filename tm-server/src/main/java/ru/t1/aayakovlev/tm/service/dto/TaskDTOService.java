package ru.t1.aayakovlev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.List;

public interface TaskDTOService extends ExtendedDTOService<TaskDTO> {

    @NotNull
    TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException;

    @NotNull
    TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException;

    @NotNull
    List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException;

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException;


    @NotNull
    TaskDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException;

    @NotNull
    TaskDTO update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException;

}
