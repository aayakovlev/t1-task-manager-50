package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.repository.model.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.model.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.model.ProjectTaskService;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskServiceImpl implements ProjectTaskService {

    @NotNull
    private final ConnectionService connectionService;

    public ProjectTaskServiceImpl(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private ProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepositoryImpl(entityManager);
    }

    @NotNull
    private TaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepositoryImpl(entityManager);
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(entityManager);
            @Nullable final Project project = projectRepository.findById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProject(project);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultTask;
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(entityManager);
            @Nullable final Project project = projectRepository.findById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProject(null);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultTask;
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final TaskRepository taskRepository = getTaskRepository(entityManager);
            @Nullable Project project = projectRepository.findById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            entityManager.getTransaction().begin();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final TaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final List<Project> projects = projectRepository.findAll(userId);
            entityManager.getTransaction().begin();
            for (@NotNull final Project project : projects) {
                taskRepository.removeAllByProjectId(userId, project.getId());
            }
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
