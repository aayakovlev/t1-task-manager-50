package ru.t1.aayakovlev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface ProjectTaskDTOService {

    @NotNull
    TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException;

    @NotNull
    TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException;

    void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException;

    void clearProjects(
            @Nullable final String userId
    ) throws AbstractException;

}
