package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.endpoint.ConnectionProvider;

public interface PropertyService extends ConnectionProvider {

    @NotNull
    String getApplicationConfig();

}
