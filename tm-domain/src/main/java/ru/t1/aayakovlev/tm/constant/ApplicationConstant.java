package ru.t1.aayakovlev.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ApplicationConstant {

    public final static int EMPTY_ARRAY_SIZE = 0;

    public final static int FIRST_ARRAY_ELEMENT_INDEX = 0;

    @NotNull
    public static final String PACKAGE_COMMANDS = "ru.t1.aayakovlev.tm.command";

    @NotNull
    public static final String PID_FILENAME = "task-manager.pid";

    @NotNull
    public static final String LIQUIBASE_CHANGELOG_FILENAME = "changeLog/changelog-master.xml";

    @NotNull
    public static final String QUEUE = "LOGGER";

    private ApplicationConstant() {
    }

}
